# Pistes d'amélioration

Un certain nombre de choses seraient pertinentes mais n'ont pas pu être réalisées dans le temps imparti au projet. Si vous souhaitez apporter votre aide ou reprendre ce projet, ce sont des pistes à explorer !

## Tutoriel

* des retours d'expériences permettant d'améliorer le tutoriel seraient les bienvenus

### _Moteur_

* Tutoriel de montage (moteurs + équipements)

## Documentation

### _Moteur_


* Caractériser la mise en parallèle de plusieurs moteurs roues
* Etude de l'électronique pour la mise en parallèle des moteurs 


## Modèle

* améliorations du modèle du triporteur

## Calculateur

* documentation et enrichissement des fonctions mathématiques disponibles
* ajout d'un switch pour passer d'un modèle à l'autre
* meilleure mise en forme (css)
* gestion des boucles de rétroaction
* proposer une Dockerfile pour une utilisation en local

## Site web & dépôt git

* tenue d'un changelog
* ressources sur le fonctionnement de Git pour des non-initié·e·s
* traduction dans d'autres langues (notamment l'anglais) pour plus d'accessibilité
