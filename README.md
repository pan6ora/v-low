![](6.Site_web/static/images/couverture.jpg)

**V-Low** est un ensemble d'outils et de ressources qui vise à accompagner le dimensionnement et la fabrication d'un triporteur électrique à partir d'éléments de récupération.

Ce projet est consultable de deux manières : 

[sous forme d'un projet gitlab](https://framagit.org/pan6ora/v-low)

[sous forme d'un site web](https://v-low.volitions.net/index.html)

[[_TOC_]]

## La vision

Dans un contexte de crise environnementale, ce projet cherche à promouvoir différentes approches :

**Récupération**

De nombreux objets produits par nos sociétés sont jetés alors même qu'ils pourraient encore servir à d'autres usages. Avec ce projet, nous cherchons à promouvoir et documenter l'utilisation d'objets ou matériaux de récupération.

**Faibles émissions**

Nous cherchons à promouvoir les vélos et carrioles comme mode de transport peu émetteur et accessible, notamment en ville. Une comparaison de l'impact d'un triporteur électrique et d'un triporteur thermique a été réalisée afin de confirmer la pertinence d'une telle solution.

**Convivialité & Low-Techs**

Ces valeurs nous semblent importantes à promouvoir. Dans ce but, ce projet cherche à être :

* accessible à tou·te·s
* compréhensible par le plus grand nombre
* dans une démarche d'autonomisation vis à vis des outils techniques
* durable

**Open Source**

Ce projet est proposé sous licence Creative Commons [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/).

Cela signifie que vous êtes libre de le copier, transformer et distribuer à condition de :

* le proposer sous la même licence
* créditer le projet original

Pour plus d'informations, se référer à la [License](./LICENSE).

## Contexte du projet

Ce projet a été initié dans le cadre du [semestre PISTE de Grenoble INP - UGA](https://ense3.grenoble-inp.fr/fr/formation/piste) sur l'année scolaire 2021-2022. Il a été initialement porté par l'association [G'Recup](http://grecup.fr/Controler/start.ctrl.php), avec pour objectif la conception d'un triporteur électrique nécessaire à leur activité.

Tous les détails de ce projet sont disponibles dans le [Contexte du projet](./3.Contexte/3.0.Accueil.md).

## Contenu du dépôt

Ce dépôt contient un certain nombre de documents créés dans des buts différents :

* [Un tutoriel](./1.Tutoriel/1.0.Accueil.md) pour la réalisation d'un triporteur électrique
* [Une documentation technique](./2.Documentation/2.0.Accueil.md) décrivant les calculs utilisés, et contenant divers annexes sur la méthodologie
* [Une description](./3.Contexte/3.0.Accueil.md) du contexte dans lequel ce projet a été mené 
* [Une documentation](./5.Modeles/README.md) pour la création de nouveaux modèles pour le dimensionnement
* [Une documentation](./6.Site_web/README.md) sur le calculateur et son déploiement

## Comment utiliser ce projet ?

### Réaliser un triporteur électrique

Rendez-vous sur la page [Tutoriel](./1.Tutoriel/1.0.Accueil.md) ! Celle-ci vous accompagnera pas-à-pas dans le dimensionnement et la conception de votre triporteur.

### Contribuer ou réutiliser ce projet

Pas de problème ! Tout le contenu de ce projet est en accès libre. Cela signifie que vous pouvez l'utiliser et l'adapter pour un autre usage.

Pour savoir comment contribuer à ce projet, se référer à la page [Contribuer](CONTRIBUTING.md).

## Pistes d'améliorations

Un certain nombre de choses seraient pertinentes mais n'ont pas pu être réalisées dans le temps imparti au projet. Si vous souhaitez apporter votre aide ou reprendre ce projet, ce sont des pistes à explorer !

Une page est [Pistes d'améliorations](CHANGELOG.md) est dédiée à cette question.


