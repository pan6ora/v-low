#include <SPI.h> // Pour la communication SPI
#include <SD.h>  // Pour la communication avec la carte SD

// Définition des variables
int niveauB ;  // Variable recueillant le niveau lu sur l'entrée A09
float UcelluleB ; // Tension de la cellule
int niveauM ;  // Variable recueillant le niveau lu sur l'entrée A0
float UcelluleM ; // Tension de la cellule
int tempo = 0 ; // variable qui vas nous servir à compter le temps écoulé
// Définition des constantes
const int digPinB = 11; // digit de sortie servant à piloter le relais
const int digPinM= 12; // digit de sortie servant à piloter le relais
const int seuil_bas = 3.0 ; // seuil de tension basse à ne pas dépasser (le seuil à ne surtout pas dépasser est 3V, passer en dessous risque d'endommager la celllule
const int seuil_haut = 4.2 ; // seuil de tension basse à ne pas dépasser
unsigned  int temps_boucle = 30; // temps de délai pour la boucle en s
unsigned  int limite_temps = (13.5*60*60)/temps_boucle; // limite de temps qui correspond à 10h
int redflagB =0;
int redflagM =0;


/* Broche CS de la carte SD */
const byte SDCARD_CS_PIN = 53 ; // TODO A remplacer suivant votre shield SD
File fichier ;
int nbr_loop = 0;




void setup() {
//partie enregistrement
  /* Initialisation du port série (debug) */
  Serial.begin(9600);
  while (!Serial) {
    ; // Wait for serial port to connect
  }

  /* Initialisation de la carte SD */
  Serial.print(F("Init SD card... "));
  if (!SD.begin(SDCARD_CS_PIN)) {
    Serial.println(F("FAIL"));
    Serial.println(F("Appuyez sur RESET"));
    for (;;); //  appui sur bouton RESET
  }
  Serial.println(F("OK")); 

  // Création fichier mesure 1
  Serial.println(F("Creation fichier..."));
  fichier = SD.open("mesures.txt", FILE_WRITE);

  if (!fichier) { // Erreur d'ouverture du fichier
    Serial.println(F("Impossible d'ouvrir le fichier"));
    Serial.println(F("Appuyez sur RESET"));
    for (;;);{} // Attend appui sur bouton RESET
  }

  Serial.println(F("Fichier cree!"));

  // Entete fichier
  if (fichier && fichier.available()<10) {
    fichier.println(F("Dataset Mesures Tension"));
    fichier.flush();
    fichier.print(F("Temps de délai entre les mesures "));fichier.println(temps_boucle);
    fichier.flush();
    fichier.println(F("Incrémentation();Tension marron (V) ;Tension bleue (V) ;Etat relais marron ();Etat ralais bleu (); tempo ; limitetemps"));
    fichier.flush();
    Serial.println(F("Entete ecrite"));
    fichier.close();
    Serial.println(F("Fichier ferme"));
  }

//partie mesure
    Serial.println(F("Initialize System"));
    pinMode(digPinB, OUTPUT);//Init pwm output
    pinMode(digPinM, OUTPUT);//Init pwm output


 fichier = SD.open("mesures.txt", FILE_WRITE);
  if (!fichier) { // Erreur d'ouverture du fichier
    Serial.println("Impossible d'ouvrir le fichier");
    Serial.println("Appuyez sur RESET");
    for (;;); // Attend appui sur bouton RESET
  }

   digitalWrite(digPinB, HIGH); //le relai est cablé en normalement ouvert, donc pour fermer le circuit il faut alimenter le relais et donc avoir le digit à high. Initialise à fermé
   digitalWrite(digPinM, HIGH);
}

void loop() {
  //partie mesure
 niveauB = analogRead(A9) ; // Lecture du signal sur l'entrée A9
 UcelluleB = niveauB * 5. / 1023. ; // Calcul de la tension lue aux bornes de la pile
 niveauM = analogRead(A0) ; // Lecture du signal sur l'entrée A0
 UcelluleM = niveauM * 5. / 1023. ; // Calcul de la tension lue aux bornes de la pile
 tempo = tempo + 1 ;

 
  if ((tempo < limite_temps )&&(UcelluleB > seuil_bas)) { 
  } 
  else {
    redflagB = 1 ;
  }
  if (redflagB==1){
    digitalWrite(digPinB, LOW);
    }

  if ((tempo < limite_temps )&&(UcelluleM > seuil_bas)) { 
  } 
  else {
    redflagM = 1 ;
  }
  if (redflagM==1){
    digitalWrite(digPinM, LOW);
    }


  //partie enregistrement 
   nbr_loop = nbr_loop + 1;

  Serial.println(nbr_loop);
  
 
  fichier.print(nbr_loop); fichier.print(';'); fichier.flush();
  fichier.print(UcelluleM); fichier.print(';'); fichier.flush();
  fichier.print(UcelluleB); fichier.print(';'); fichier.flush();
  fichier.print(digitalRead(digPinM)); fichier.print(';'); fichier.flush();
  fichier.print(digitalRead(digPinB)); fichier.print(';'); fichier.flush();
  fichier.print(tempo);fichier.print(';'); fichier.flush();
  fichier.print(limite_temps );fichier.println(';'); fichier.flush();
    
  Serial.print(nbr_loop);Serial.print("\t"); Serial.print("Tension Marron:");Serial.print(UcelluleM);
  Serial.print("Tension Bleu:");Serial.print(UcelluleB);Serial.print("\t");
  Serial.print("Etat relais marront : ");Serial.print(digitalRead(digPinM));Serial.print("\t");
  Serial.print("Etat relais bleu : ");Serial.print(digitalRead(digPinB)); Serial.print("\t");
  Serial.print("tempo : ");Serial.print(tempo);Serial.print("\t");
  Serial.print("limite_temps : ");Serial.print(limite_temps);Serial.print("\t");

  
  delay(temps_boucle*1000);

}
