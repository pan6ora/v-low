# Chassis
## masse (kg)
```
Mc = 200
```
Masse totale du châssis et de son chargement.

## coefficient de frottement
```
f = 1.0
```
Valeur représentant les frottements mécaniques entre le sol et le véhicule.

## coefficient de traînée
```
Cx = 0.2
```
Valeur représentant la résistance opposée par l'air.

## longueur pédale
```
rpedale = 0.17
```

## rayons des roues
```
rroue = 0.33
```
Rayon des roues. Celui-ci permet de calculer les valeurs de couple et la vitesse de la roue.

# Utilisation
## pente moyenne (%)
```
pmoy = 1
```
La pente moyenne dépend du profil du parcours du cycliste. Pour une ville de plaine, cette valeur est le plus souvent entre 0 et 15%. Pour une meilleure précision, [le site suivant](http://www.velomath.fr/calcul_peq.php) permet de calculer la pente moyenne à partir d'une trace GPX.

## pente maximale (%)
```
pmax = 10
```
Pourcentage maximal de pente que pourra parcourir le cycliste dans le cas le plus critique. À titre indicatif, la [valeur maximale de pente enregistrée en France pour une rue](https://routes.fandom.com/wiki/Rues_et_voies_%C3%A0_fortes_pentes) est de 33%.

## vent moyen (km/h)
```
vmoy = 10
```
Vitesse moyenne de vent que devra affronter le cycliste au cours de son trajet. Le site [meteociel](https://www.meteociel.fr/obs/classement.php?mode=11&all=1) recense la vitesse moyenne de vent enregistrée par des stations météos en France.

## vent maximal (km/h)
```
vmax = 50
```
Vitesse maximale de vent que le cycliste pourra affronter. À titre indicatif Le vent violent (tel que définit pour les alertes vigilances en France) se caractérise par une vitesse atteinte de 80 km/h en vent moyen, et 100 km/h en rafale. Une valeur raisonnable est de 50km/h pour un véhicule aussi lourd qu'un triporteur.

## distance maximale (km)
```
dmax = 40
```
Trajet le plus long que pourra parcourir le cycliste entre deux recharges complètes de la batterie.

## vitesse moyenne (km/h)
```
Vmoy = 12
```
Allure régulière souhaitée lors du déplacement. Pour des questions de sécurité, cette vitesse ne devrait pas être supérieure à 25km/h. Une valeur raisonnable serait entre 10 et 20km/h (selon la masse du triporteur).

## vitesse minimale (km/h)
```
Vmin = 8
```
Allure minimale souhaitée dans le cas le plus contraignant (vent maximal et pente maximale). 5 à 10km/h semble être une valeur raisonnable.

## temps de démarrage max (s)
```
tdem = 10
```
Temps que mettra le cycliste pour atteindre la vitesse minimale dans le pire des cas (vent maximal et pente maximale). Entre 5 et 15 secondes semble être une valeur raisonnable.

## autonomie (h)
```
t = dmax/Vmoy
```
Durée d'un trajet entre deux recharges. Elle est calculée à partir de la distance maximale et de la vitesse moyenne.

## accélération au démarrage (m/s)
```
adem = Vmin/(tdem*3.6)
```
L'accélération au démarrage est calculée à partir du temps de démarrage et de la vitesse minimale.

# Cycliste
## masse maximale (kg)
```
Mh = 90
```
Masse maximale du cycliste.

## puissance max instantanée (W)
```
Phmax = 300
```
Puissance maximale que peut développer le cycliste durant une seconde. Se referrer aux ordres de grandeurs [suivants](https://www.velochannel.com/cest-quoi-la-puissance-en-cyclisme-32808) pour déterminer votre puissance en fonction de votre forme physique et de votre masse. 

## puissance moyenne (W)
```
Phmoy = 100
```
Puissance  que peut déployer le cycliste durant une longue durée. Se référer aux ordres de grandeurs [suivants](https://www.velochannel.com/cest-quoi-la-puissance-en-cyclisme-32808) pour déterminer votre puissance en fonction de votre forme physique et de votre masse.

# Transmission pédale/roue
## rapport max
```
Rcmax = 1
```
Efficacité de la transmission entre le moteur et la roue. Dans le cas d'un moteur roue, ce rendement est maximal (= 1).

## rapport min
```
Rcmin = 1
```

# Efforts mécaniques
## force d'accélération au démarrage
```
Fa = (Mh+Mc)*adem
```
Force nécessaire pour faire accélérer le triporteur selon l'accélération demandée 

## force à vitesse minimale en régime stationnaire
```
Fvmin = (pmax+f)*(Mc+Mh)*(1/36)+(250/11664)*Cx*Math.pow(Vmin+vmax,2)
```
Force nécessaire pour déplacer le triporteur dans les pires conditions (pente maximale, vent maximal). Dans ce calcul, on prend en compte la vitesse minimale attendue (Vmin) au lien de la vitesse moyenne attendue.

## force à vitesse moyenne en régime stationnaire
```
Fmoy = (pmoy+f)*(Mc+Mh)*(1/36)+(250/11664)*Cx*Math.pow(Vmoy+vmoy,2)
```
Force nécessaire pour faire avancer le triporteur.

## puissance au démarrage
```
Pdem = Vmin*(Fvmin+Fa)
```

## puissance à vitesse moyenne en régime stationnaire
```
Pmoy = Vmoy*Fmoy
```
Cette puissance devra être la somme des puissances moyennes du moteur et du cycliste.

## puissance maximale
```
Pmax = Math.max(Pdem,Pmoy)
```
Cette puissance devra être la somme des puissances maximales du moteur et du cycliste. Dans ce calcul, on prend en compte la vitesse minimale attendue (Vmin) au lien de la vitesse moyenne attendue.

## vitesse moyenne roue (rad/s)
```
Rrmoy = Vmoy/(rroue*3.6)
```
Vitesse de rotation de la roue

## vitesse minimale roue (rad/s)
```
Rrmin = Vmin/(rroue*3.6)
```
Vitesse de rotation de la roue à la vitesse minimale attendue

## couple max
```
Cmax = Math.max(Pmax/Rrmin,Pmoy/Rrmoy)
```
Couple nécessaire au niveau de la roue pour avancer à la vitesse minimale dans les pires conditions (pente maximale, vent maximal).

# Transmission moteur/roue
## rendement
```
rm = 1
```
Efficacité de la transmission entre le moteur et la roue. Dans le cas d'un moteur roue, ce rendement est maximal (= 1).

# Moteur
## puissance max (W)
```
Pm = Math.max(Pdem-Phmax,Pmoy-Phmoy)
```

## puissance moyenne (W)
```
Pmmoy = Pmoy-Phmoy
```

## couple max (Nm)
```
Cmmax = Math.max(Pm/Rrmin,Pmmoy/Rrmoy)
```

## voltage
```
Vm = 18
```

## ampérage
```
I = Pmmoy/Vm
```

## pic courant
```
Imax = Pm/Vm
```

# Batterie
## taille série
```
S = Math.round(Vm/Vp)
```
Nombre de cellules à câbler en série.

## voltage
```
Vb = Vp*S
```
Voltage total de la batterie.

## autonomie (Ah)
```
A = Pmmoy/Vb*t
```
Autonomie totale de la batterie.

## nb de séries
```
P = Math.max(Math.round(A/A_pile),Math.round(Imax/Ipmax))
```
Nombre de séries de cellules à câbler en parallèle.

## volume (cm³) (cellules uniquement)
```
Volb = S*P*Volp
```
Volume occupé par les cellules.

## courant nominal (A)
```
Ib = Ip*P
```

## nombre de piles
```
nb_piles = S*P
```
Nombre de piles nécessaires à la réalisation de la batterie

# Piles
## tension nominale
```
Vp = 3.6
```

Tension d'une seule pile. Cette tension est plus plus souvent aux alentours de 3.6V.
## courant nominal (A)
```
Ip = 20
```
Courant de décharge idéal de la batterie.

## intensité max
```
Ipmax = 60
```
Courant maximal que peut délivrer la pile.

## autonomie (Ah)
```
A_pile = 9.9
```
Autonomie d'une seule pile en Ah.

## volume (cm³)
```
Volp = 16.53
```
Volume occupé par une seule pile (`largeur x hauteur x longueur`)
