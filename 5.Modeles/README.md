# Description des Modèles

Les **modèles** sont des **fichiers rédigés en markdown** selon une syntaxe spécifique, qui **décrivent un système** à travers deux choses:

* un ensemble de variables
* des relations entre ces variables

Ils permettent ensuite de générer une page appelée **calculateur**, qui permet de rentrer facilement les valeurs d'entrées et d'observer l'évolution des variables de sortie.

## Ajouter un modèle

Pour ajouter un nouveau modèle, il suffit d'ajouter dans ce dossier un fichier markdown respectant la syntaxe décrite ci-dessous.

## Format du fichier

On explicite ici la façon dont est construit un modèle, et les limites de celui-ci. Une autre façon d'appréhender cette syntaxe est de regarder [le modèle de triporteur existant](triporteur.md).

### valeur d'une donnée

Il y a 3 façons de représenter une donnée :

* indiquer **uniquement la notation**. La donnée sera considérée comme une **variable d'entré** que l'utilisateur doit renseigner.

ex:
```
Vmax
```

* indiquer une **valeur par défaut**. La donnée sera considérée comme une **variable d'entré** à renseigner, mais possédera une valeur par défaut.

ex:
```
Vmax = 10
```

* indiquer un **calcul**. La donnée sera considérée comme une **variable de sortie** Ce calcul peut impliquer des nombre, des entrées, des sorties et des fonctions mathématiques classiques.

ex:
```
Vmax = 10*(Pmax/I)
```

Pour plus d'informations sur l'écriture des calculs, se référer à la section [calculs](#calculs).

## Sections

Le fichier est découpé de la manière suivante :

```
# Catégorie
## donnée
`calcul`
description
```

Pour voir ce découpage à l'œuvre, consulter le modèle par défaut `triporteur.md`.

## Calculs

Les calculs sont réalisés dans le language Javascript. Toutes les fonctions disponibles dans celui-ci sont donc utilisables. Une liste des fonctions existantes est disponible [sur le site de mozilla](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Math).

Attention ! Le calculateur est assez capricieux, et une mauvaise syntaxe pourrait entraîner un mauvais fonctionnement. Bien lire la section [limitations](#limitations)

### Ordre des calculs

Les sorties sont calculées de haut en bas. Autrement dit, un calcul ne peut prendre en compte une valeur calculée plus loin dans le fichier. Cela n'est pas valable pour les entrées.

par exemple, l'ordre suivant est valable :

```
B = A + 5

V = A*B     V doit être calculé après B

A = 12      A peut être placé à la fin car c'est une entrée
```

mais l'ordre suivant ne l'est pas :

```
A = 12

V = A*B     V doit être calculé après B !

B = A + 5
```

Pour éviter cette erreur, on conseille de décrire le système en partant des données d'entrée et en descendant petit à petit vers les dernières valeurs de sortie.

### fonctions classiques

Racine carrée : `V = Math.sqrt(x)`

Carré : `V = Math.pow(x,2)`

PI : `V = Math.PI()`

Maximum : `V = Math.max(x,y)`

### limitations

*  il doit y avoir un espace autour du `=`. Par exemple, `V = x*y` est valide, mais `V=x*y` ne l'est pas
*  il ne doit pas y avoir d'espaces dans la formule. Par exemple, `V = x*y` est valide, mais `V = x * y` ne l'est pas

## Description

La description est faite en Markdown. Toute la syntaxe est à priori supportée. 

Attention ! Les titres doivent commencer à partir de ### afin de ne pas interférer avec les catégories et les données.
