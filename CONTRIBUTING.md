# Contribuer

Il y a pleins de bonnes raison de contribuer à ce projet :

* proposer une amélioration dans le modèle du triporteur
* proposer de nouveaux modèles, pour de nouveaux systèmes
* améliorer les tutoriels et la documentation
* faire des retours d'expérience...

Pour cela, plusieurs solutions sont possibles selon que vous êtes familiers de l'utilisation de git ou non.

## Nous contacter par email

Si vous n'êtes pas familier avec git, une solution simple est d'envoyer un mail à l'adresse remy.lecalloch@grenoble-inp.org.

Indiquez les modifications que vous pensez nécessaires, en joignant éventuellement les fichiers modifiés.

## La méthode Git: faire une requête de merge

Le projet étant actuellement hébergé sur l'instance Gitlab de Grenoble INP - Ensimag, il n'est pas possible pour des personnes extérieures à Grenoble INP de proposer des modifications sur le dépôt.

À terme, ce dépôt devrait passer dans une instance public de Gitlab, ce qui permettra à tout un chacun de participer.