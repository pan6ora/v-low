#!/bin/sh

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
url="https://v-low.volitions.net"
css_url=$url/6.Site_web/static/css/main.css

# Creation d'une liste des fichiers Markdown
# format: create_pages_list
create_pages_list ()
{
  tmp=$(mktemp)
  find . -mindepth 2 -maxdepth 2 -type f \( -name "*.md" -o -name "*.html" \) | tree --fromfile . -if --noreport | sed -e "s+\./\./+\./+" -e "s+\.md++" -e "s+\.html++" >> $tmp
  cat $tmp
  rm $tmp
}

# Create a list of all markdown paths
# format: create_md_files_list
create_md_files_list ()
{
  find . -type f -name "*.md"
}

# Create a markdown list from a folder structure, adding base URL
# format: create_md_list <path_list> <website_url>
create_md_list () 
{
  echo "* [Accueil]($url/index.html)"
  {
    read
    while read f ; do
      if [ "$f" != "./." ]; then
        f=$(echo $f | sed s+\./++)
        if [[ $f == *"/"* ]]; then
          echo "$f" | sed -e 's+[^/]*/+  +g' -e "s+[^ ]*$+* [&]($2/$f.html)+"
        else
          echo "$f" | sed -e 's+[^/]*/+  +g' -e "s+[^ ]*$+* [&]()+"
        fi
      fi
    done
  } < $1
}

# Generate an html <nav> element from a markdown file
# format: create_html_nav <markdown_file.md>
create_html_nav () 
{
    {
    echo '<nav id="mainNav"><nav id="mainNavContent">'
    cat $1
    echo '</nav></nav>'
    } | pandoc -f markdown -t html
}
# Convert all markdown links to html links
# format: links_to_html <path_list>
links_to_html ()
{
  while read f ; do
  sed -i 's+\(\[[^(]*\]([^(]*\)\.md)+\1.html)+g' $f
  done < $1
}
# Convert all index.md to index.html
# format: pages_to_html <path_list> 
pages_to_html ()
{
  while read f ; do
  html_name=$(echo $f | sed s+\.md$+\.html+)
  pandoc "$f" -s --toc --mathjax -o "$html_name" --include-before-body=menu.html --css=$css_url;
  rm "$f";
  done < $1
}

# generation d'une page html par modèle dans le dossier calculateurs
mkdir -p calculateurs
python3 generateur.py
# supression du contenu public
rm -rf $script_dir/public
# creation d'une copie temporaire du dépôt
temp=$(mktemp -d)
cp -r $script_dir/../* $temp/
# passage dans la copie
cd $temp
# suppression du sommaire gitlab dans le README
sed -i "s/\[\[_TOC_\]\]//" README.md
# déplacement des calculateurs dans le bon dossier
mv 6.Site_web/calculateurs 4.Calculateurs
rm -rf 6.Site_web/calculateurs
# creation du menu latéral
create_pages_list > list_temp 
create_md_list list_temp $url | create_html_nav > menu.html
# passage en html de tous les fichiers markdown
create_md_files_list > md_list_temp
links_to_html md_list_temp
pages_to_html md_list_temp
# rennomage du README.html en index.html
cp README.html index.html
# mise à jour des droits des fichiers
chmod -R a+r *
# suppression des fichiers temporaires
rm -r list_temp md_list_temp $script_dir/calculateurs
# déplacement do dossier temporaire dans Site_web/public
mkdir -p $script_dir/public
cp -r * $script_dir/public/
