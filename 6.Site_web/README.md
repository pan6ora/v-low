# Documentation du site web

Ce fichier documente la génération du site web  et du calculateur à partir du dépôt git.

## Connaissances préalables

Cette partie implique un certain nombre d'outils informatiques. Elle demande donc des connaissances plus spécifiques que le reste du projet. Nous tenterons ici de vulgariser au maximum les choses mises en place, mais ce document n'a pas vocation à être un cours sur des languages ou méthodes informatiques.

La compréhension sera fortement facilitée si vous avez une connaissance basique :

* du fonctionnement de Git et de Gitlab
* du language Python
* de Docker
* des scripts bash

## Contenu & Articulation

[generateur.py](generateur.py)

Ce fichier contient du code dans le language Python. Il permet à partir d'un modèle au format de ceux contenus dans le dossier [5.Modeles](../5.Modeles/README.md) de créer une page web au format html contenant le calculateur associé.

[generateur.sh](generateur.sh)

Ce fichier est un script, c'est à dire une série de commandes. Il permet à partir du dépôt de générer le contenu d'un site web.

Il se charge notamment de:

* créer le menu latéral à partir de l'arborescence du dépôt
* convertir chaque fichier markdown en une page html
* créer les calculateurs en utilisant [generateur.py](generateur.py) pour chaque modèle du dossier [5.Modeles]()

[.gitlab-ci.yml](.gitlab-ci.yml)

Ce fichier contient une "recette de cuisine" permettant à Gitlab de générer le site web automatiquement.

Plus précisément, ce fichier permet de créer un [conteneur Docker](https://www.ontrack.com/fr-fr/blog/conteneurs-docker), et d'exécuter le script de génération du site web à l'intérieur de celui-ci.

[.gitignore](.gitignore)

Ce fichier contient une liste de dossiers et de fichiers qui doivent être ignorés par Git.

Si vous travaillez avec un clone local de ce dépôt sur votre ordinateur, vous pouvez tester la génération du site web en exécutant le script [generateur.sh](generateur.sh). Celui-ci génère un dossier `public` contenant le site web.

Mais nous n'avons aucun intérêt à stocker ce dossier sur le dépôt en ligne, puisque n'importe qui peut le générer à partir du script. On utilise donc le fichier [.gitignore](.gitignore) pour signaler à Git qu'il faut ignorer ce dossier.

## Génération locale

Le contenu du site web est disponible [en ligne](https://v-low.volitions.net/). Néanmoins vous pourriez avoir envie de le générer en local sur votre ordinateur (pour faire des tests par exemple).
Pour générer le contenu du site web sur votre ordinateur, la procédure est la suivante :

* télécharger ou cloner le contenu du [dépôt](https://framagit.org/pan6ora/v-low)
* installer si nécessaire **python3** et ses dépendances :
  * pypandoc, jinja2
* installer si nécessaire **pandoc**
* executer le script **generateur.sh** présent dans le dossier 6.Site_web

Une fois l'execution terminée, le site généré est disponible dans le dossier 6.Site_web/public


