# Import des bibliothèques nécessaires
import os
import pypandoc
from jinja2 import Template, Environment, FileSystemLoader

def extraction_fichier(nom_fichier='triporteur.md'):
    # Entrée: contenu d'un modèle
    # Sortie: un objet Modele
    with open('../5.Modeles/'+nom_fichier) as f:
        nom_modele = os.path.splitext(nom_fichier)[0]
        modele = Modele(nom_modele)
        contenu = []
        for ligne in f:
            if ''.join(ligne[:2])=="# ":
                if contenu != []:
                    categorie = extraction_categorie(contenu, modele)
                    modele.ajouter(categorie)
                contenu = []
            contenu.append(ligne)
        if contenu != []:
            categorie = extraction_categorie(contenu, modele)
            modele.ajouter(categorie)
        contenu = []
    return modele

def extraction_categorie(contenu, modele):
    # Entrée: contenu d'une catégorie
    # Sortie: un objet Categorie
    nom = str(contenu[0][2:-1])
    categorie = Categorie(nom)
    sous_contenu = []
    for ligne in contenu[1:]:
        if ''.join(ligne[:3])=="## ":
            if sous_contenu != []:
                donnee = extraction_donnee(sous_contenu)
                categorie.ajouter(donnee)
                modele.ajouter(donnee)
            sous_contenu = []
        sous_contenu.append(ligne)
    if sous_contenu != []:
        donnee = extraction_donnee(sous_contenu)
        categorie.ajouter(donnee)
        modele.ajouter(donnee)
    sous_contenu = []
    return categorie

def extraction_donnee(contenu):
    # Entrée: contenu d'une donnée
    # Sortie: un objet Donnee   
    nom = str(contenu[0][3:-1])
    formule = contenu[2][:-1].split(' ')
    notation = formule[0]
    calcul = "0" if len(formule)==1 else formule[2]
    description = '\n'.join(contenu[4:])
    try:
        calcul = float(calcul)
        return Entree(nom, notation, description, calcul)
    except ValueError:
        return Sortie(nom, notation, description, calcul)

class Modele(object):
    # un objet Modele est la 'traduction' en objets d'un fichier modèle en markdown
    def __init__(self, nom):
        self.nom = nom
        self.categories = []
        self.entrees = []
        self.sorties = []
    def ajouter(self, objet):
        type = objet.__class__.__name__
        if(type=="Categorie"):
            self.categories.append(objet)
        elif(type=="Entree"):
            self.entrees.append(objet) 
        elif(type=="Sortie"):
            self.sorties.append(objet)
        else:
            raise Exception("Unknown object type")

class Categorie(object):
    # un objet Categorie est la 'traduction' en objets d'une catégorie issue d'un modèle en markdown
    def __init__(self, nom):
        self.nom = nom
        self.donnees = []
    def ajouter(self, donnee):
        self.donnees.append(donnee)

class Donnee(object): 
    # un objet Donnee est la 'traduction' en objets d'une donnée issue d'un modèle en markdown
    def __init__(self, nom, notation, description, valeur):
        self.nom = nom
        self.notation = notation
        self.description = pypandoc.convert_text(description, 'html', format='md')
        self.valeur = valeur

class Entree(Donnee):
    # classe fille de Donnee
    def __init__(self, nom, notation, description, valeur): 
        super().__init__(nom, notation, description, valeur)

class Sortie(Donnee):
    # classe fille de Donnee
    def __init__(self, nom, notation, description, calcul): 
        self.calcul = calcul
        super().__init__(nom, notation, description, -1)
    def calculer(self):
        valeur = 0
        return valeur


""" Génération du site web """

class Generateur(object):
    def __init__(self):
        self.env = Environment(loader=FileSystemLoader('templates'))
        self.render_page()

    def render_page(self):
        modele = extraction_fichier()
        template = self.env.get_template('calculateur.html')
        with open("calculateurs/"+modele.nom+'.html', 'w+') as file:
            html = template.render(modele=modele)
            file.write(html)


if __name__ == "__main__":
    Generateur()